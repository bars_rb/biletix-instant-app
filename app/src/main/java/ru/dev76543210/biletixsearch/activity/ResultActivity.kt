package ru.dev76543210.biletixsearch.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_result.*
import ru.dev76543210.biletixsearch.R
import ru.dev76543210.biletixsearch.adapter.TrainDetailsAdapter
import ru.dev76543210.biletixsearch.model.SearchRequest
import ru.dev76543210.biletixsearch.model.Train
import ru.dev76543210.biletixsearch.viewmodel.ResultViewModel


class ResultActivity : AppCompatActivity() {

    private lateinit var resultViewModel : ResultViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        resultViewModel = ViewModelProvider(this).get(ResultViewModel::class.java)
        resultViewModel.view = this

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        rvTrains.layoutManager = layoutManager

        val intent = intent
        val searchRequest = intent.getSerializableExtra("searchRequest") as SearchRequest

        resultViewModel.requestTrains(searchRequest)
    }

    fun showTrains(trains: List<Train>) {
        val adapter = TrainDetailsAdapter(trains, this)
        rvTrains.adapter = adapter
    }

    fun showError(description: String) {
        tvError.text = description
        tvError.visibility = View.VISIBLE
    }

    fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}