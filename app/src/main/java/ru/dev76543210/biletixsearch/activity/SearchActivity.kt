package ru.dev76543210.biletixsearch.activity

import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import ru.dev76543210.biletixsearch.R
import ru.dev76543210.biletixsearch.databinding.ActivitySearchBinding
import ru.dev76543210.biletixsearch.viewmodel.SearchViewModel

class SearchActivity : AppCompatActivity() {

    private var binding : ActivitySearchBinding? = null
    private lateinit var searchViewModel : SearchViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        searchViewModel = ViewModelProvider(this).get(SearchViewModel::class.java)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        binding?.searchParameters = searchViewModel.searchRequest
    }


    fun search(view: View) {
        val resultActivityIntent = Intent(this, ResultActivity::class.java)
        resultActivityIntent.putExtra("searchRequest", searchViewModel.searchRequest)
        startActivity(resultActivityIntent)
    }


    fun selectDate(view: View) {
        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, day -> run {
            searchViewModel.setDate(year, month, day)
            binding?.invalidateAll()
        }
        }, searchViewModel.getYear(), searchViewModel.getMonth()-1, searchViewModel.getDay())
        dpd.show()
    }
}