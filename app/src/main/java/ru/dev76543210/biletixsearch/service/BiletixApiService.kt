package ru.dev76543210.biletixsearch.service

import retrofit2.Call
import retrofit2.http.Body

import retrofit2.http.POST
import ru.dev76543210.biletixsearch.model.SearchQuery
import ru.dev76543210.biletixsearch.model.TrainListResult

interface BiletixApiService {
    @POST("searchOffers")
    fun searchOffers(@Body q: SearchQuery): Call<TrainListResult>
}