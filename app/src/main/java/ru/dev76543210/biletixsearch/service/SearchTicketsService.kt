package ru.dev76543210.biletixsearch.service

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.dev76543210.biletixsearch.model.SearchQuery
import ru.dev76543210.biletixsearch.model.TrainListResult


class SearchTicketsService() {
    fun searchRailwayTickets(from: String, to: String, date: String) : TrainListResult {
        val originDestinationInformation = SearchQuery.Information(date, SearchQuery.TimeRange("00:00", "24:00"), SearchQuery.Location(to), SearchQuery.Location(from))
        val searchQuery = SearchQuery("rail.biletix.ru.android",
            SearchQuery.Query(arrayOf(originDestinationInformation)),
            "railroad")


            val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl("https://railway.biletix.ru/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            val biletixApiService = retrofit.create<BiletixApiService>(BiletixApiService::class.java)
            val response: Response<TrainListResult> = biletixApiService.searchOffers(searchQuery).execute()

            return response.body()!!
        }
}
