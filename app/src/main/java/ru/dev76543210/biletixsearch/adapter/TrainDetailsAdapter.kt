package ru.dev76543210.biletixsearch.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.FlexboxLayout
import ru.dev76543210.biletixsearch.R
import ru.dev76543210.biletixsearch.model.Train

class TrainDetailsAdapter(private val trains: List<Train>, context: Context) : RecyclerView.Adapter<TrainDetailsAdapter.TrainDetailsViewHolder>() {

    internal val trainCarPriceView = mutableMapOf<Train, List<TrainCarPriceView>>()

    init {
        trainCarPriceView.clear()
        for (train in trains) {
            val viewsList = mutableListOf<TrainCarPriceView>()

            for (carPrice in train.carTypes) {
                viewsList.add( TrainCarPriceView(carPrice, context))
            }
            trainCarPriceView[train] = viewsList
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrainDetailsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TrainDetailsViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: TrainDetailsViewHolder, position: Int) {
        val train: Train = trains[position]
        holder.bind(train)
    }

    override fun getItemCount(): Int = trains.size




    inner class TrainDetailsViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.train_details, parent, false)) {
        private var tvNumberFull: TextView? = null
        private var tvCategory: TextView? = null
        private var tvDepart: TextView? = null
        private var tvArrival: TextView? = null
        private var tvTripDuration: TextView? = null
        private var fbCarPrices: FlexboxLayout? = null

        init {
            tvNumberFull = itemView.findViewById(R.id.tvNumberFull)
            tvCategory = itemView.findViewById(R.id.tvCategory)
            tvDepart = itemView.findViewById(R.id.tvDepart)
            tvArrival = itemView.findViewById(R.id.tvArrival)
            tvTripDuration = itemView.findViewById(R.id.tvTripDuration)
            fbCarPrices = itemView.findViewById(R.id.fbCarPrices)
        }

        fun bind(train: Train) {
            tvNumberFull?.text = train.numberFull
            tvCategory?.text = train.categories[0].name
            tvDepart?.text = train.passTripInfo.depart.split(" ")[1]
            tvArrival?.text = train.passTripInfo.arrival.split(" ")[1]
            tvTripDuration?.text = train.passTripInfo.tripDuration

            fbCarPrices?.removeAllViews()
            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT
            )
            layoutParams.setMargins(8, 8, 8, 0)
            for (trainCarsPrice in trainCarPriceView[train]!!) {
                fbCarPrices?.addView(trainCarsPrice.getView(), layoutParams)
            }
        }

    }



    class TrainCarPriceView(carType: Train.CarType, context: Context) {
        private var trainCarPriceLayout: View? = null

        init {
            trainCarPriceLayout = LayoutInflater.from(context)
                .inflate(R.layout.train_details_car_price, null, false)

            val tvCarType = trainCarPriceLayout?.findViewById<TextView>(R.id.tvCarType)
            val tvPrice = trainCarPriceLayout?.findViewById<TextView>(R.id.tvPrice)
            tvCarType?.text = carType.category.code
            tvPrice?.text = carType.priceDetails.totalMin

        }

        fun getView(): View? {
            return trainCarPriceLayout
        }

    }

}