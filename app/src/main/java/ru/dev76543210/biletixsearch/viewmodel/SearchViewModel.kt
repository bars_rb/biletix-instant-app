package ru.dev76543210.biletixsearch.viewmodel

import androidx.lifecycle.ViewModel
import ru.dev76543210.biletixsearch.model.SearchRequest
import java.util.*

class SearchViewModel : ViewModel() {
    val searchRequest : SearchRequest = SearchRequest("2000000", "Москва", "2004000", "Санкт-Петербург", "27.06.2020")
    init {
        val c = Calendar.getInstance()
        val currentYear = c.get(Calendar.YEAR)
        val currentMonth = c.get(Calendar.MONTH)+1
        val currentDay = c.get(Calendar.DAY_OF_MONTH)
        val strDay = if (currentDay < 10) "0$currentDay" else "$currentDay"
        val strMonth = if (currentMonth < 10) "0$currentMonth" else "$currentMonth"
        searchRequest.Date = "$strDay.${strMonth}.$currentYear"
    }

    fun setDate(year: Int, month: Int, day: Int) {
        val strDay = if (day < 10) "0$day" else "$day"
        val intMonth = month + 1
        val strMonth = if (intMonth < 10) "0$intMonth" else "$intMonth"
        searchRequest.Date = "$strDay.${strMonth}.$year"
    }

    fun getYear() : Int {
        return searchRequest.Date.substring(6).toInt()
    }

    fun getMonth() : Int {
        return searchRequest.Date.substring(3, 5).toInt()
    }

    fun getDay() : Int {
        return searchRequest.Date.substring(0, 2).toInt()
    }
}