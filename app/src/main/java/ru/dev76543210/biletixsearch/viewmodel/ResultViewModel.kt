package ru.dev76543210.biletixsearch.viewmodel

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import ru.dev76543210.biletixsearch.activity.ResultActivity
import ru.dev76543210.biletixsearch.model.SearchRequest
import ru.dev76543210.biletixsearch.service.SearchTicketsService

class ResultViewModel : ViewModel() {

    lateinit var view: ResultActivity
    private val searchTicketsService = SearchTicketsService()

    private fun prepareDate(date: String): String {
        return date.substring(6, 10) + "-" + date.substring(3, 5) + "-" + date.substring(0, 2)
    }

    private fun getTrains(from: String, to: String, date: String) {
        val result = CoroutineScope(Dispatchers.IO).async {
            searchTicketsService.searchRailwayTickets(from, to, date)
        }
        CoroutineScope(Dispatchers.Main).launch {
            val requestResult = result.await()
            view.hideProgressBar()

            if(requestResult.error == null) {
                view.showTrains(requestResult.result.trainList[0])
            } else {
                view.showError(requestResult.error[0].description)
            }

        }
    }

    fun requestTrains(searchRequest: SearchRequest) {
        getTrains(searchRequest.SrcCityCode, searchRequest.DstCityCode, prepareDate(searchRequest.Date))
    }
}