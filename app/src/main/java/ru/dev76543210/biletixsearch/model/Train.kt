package ru.dev76543210.biletixsearch.model

import com.google.gson.annotations.SerializedName

data class Train(
    val numberFull: String,
    val brand: String,
    @SerializedName("category")
    val categories: List<TrainCategory>,
    val carTypes: List<CarType>,
    val passTripInfo: PassTripInfo
) {
    inner class TrainCategory(val code: String, val name: String)
    inner class CarType(val class_: String, val category: CarCategory, val priceDetails: CarPriceDetails)
    inner class CarCategory(val code: String)
    inner class CarPriceDetails(val totalMin: String)
    inner class PassTripInfo(val depart: String, val arrival: String, val tripDuration: String)
}
