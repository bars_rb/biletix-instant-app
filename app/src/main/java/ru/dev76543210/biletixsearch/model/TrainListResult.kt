package ru.dev76543210.biletixsearch.model

data class TrainListResult(
    val result: Result,
    val error: List<ResponseError>?
) {
    class ResponseError (
            val code: String,
            val status: String,
            val description: String,
            val descID: String,
            val message: String
    )
    class Result (
        val trainList : List<List<Train>>
    )

}


