package ru.dev76543210.biletixsearch.model

import com.google.gson.annotations.SerializedName

class SearchQuery (val altDomain: String,
                   @SerializedName("q")
                   val query: Query,
                   @SerializedName("rqType")
                   val requestType: String) {

    class Query (@SerializedName("OriginDestinationInformation") val information: Array<Information>)

    class Information (val DepartureDate: String,
                 val DepartureTimeRange: TimeRange,
                 val DestinationLocation: Location,
                 val OriginLocation: Location
    )

    class TimeRange (val From: String, val To: String)

    class Location (val LocationCode: String)

}