package ru.dev76543210.biletixsearch.model

import java.io.Serializable

class SearchRequest (var SrcCityCode : String,
                     var SrcCityName : String,
                     var DstCityCode : String,
                     var DstCityName : String,
                     var Date : String) : Serializable